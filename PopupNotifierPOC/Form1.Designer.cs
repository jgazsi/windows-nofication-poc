﻿
namespace PopupNotifierPOC
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ActivateNotification1Button = new System.Windows.Forms.Button();
            this.activateNotificationButton1 = new System.Windows.Forms.Button();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.SuspendLayout();
            // 
            // ActivateNotification1Button
            // 
            this.ActivateNotification1Button.Location = new System.Drawing.Point(241, 113);
            this.ActivateNotification1Button.Name = "ActivateNotification1Button";
            this.ActivateNotification1Button.Size = new System.Drawing.Size(157, 23);
            this.ActivateNotification1Button.TabIndex = 0;
            this.ActivateNotification1Button.Text = "Show Tulpep Notification";
            this.ActivateNotification1Button.UseVisualStyleBackColor = true;
            this.ActivateNotification1Button.Click += new System.EventHandler(this.button1_Click);
            // 
            // activateNotificationButton1
            // 
            this.activateNotificationButton1.Location = new System.Drawing.Point(241, 84);
            this.activateNotificationButton1.Name = "activateNotificationButton1";
            this.activateNotificationButton1.Size = new System.Drawing.Size(157, 23);
            this.activateNotificationButton1.TabIndex = 1;
            this.activateNotificationButton1.Text = "Show NotifyIcon";
            this.activateNotificationButton1.UseVisualStyleBackColor = true;
            this.activateNotificationButton1.Click += new System.EventHandler(this.activateNotificationButton1_Click);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(631, 267);
            this.Controls.Add(this.activateNotificationButton1);
            this.Controls.Add(this.ActivateNotification1Button);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Popup Notifier";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ActivateNotification1Button;
        private System.Windows.Forms.Button activateNotificationButton1;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
    }
}

