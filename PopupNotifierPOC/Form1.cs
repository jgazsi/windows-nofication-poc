﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tulpep.NotificationWindow;

namespace PopupNotifierPOC
{
    public partial class Form1 : Form
    {
        private string notificationTitle = "Important RedRover Notification.";
        private string notificationContent = "Click here!";

        public Form1()
        {
            InitializeComponent();
            SetBalloonTip();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Invoke((MethodInvoker)delegate
            {
                PopupNotifier pop = new PopupNotifier();
                pop.TitleText = notificationTitle;
                pop.ContentText = notificationContent;
                pop.Popup();
            });
        }

        private void activateNotificationButton1_Click(object sender, EventArgs e)
        {
            notifyIcon1.Visible = true;
            notifyIcon1.ShowBalloonTip(30000);
        }

        private void SetBalloonTip()
        {
            notifyIcon1.Icon = SystemIcons.Exclamation;
            notifyIcon1.BalloonTipTitle = notificationTitle;
            notifyIcon1.BalloonTipText = notificationContent;
            notifyIcon1.BalloonTipIcon = ToolTipIcon.Error;
            notifyIcon1.BalloonTipClicked += NotifyIcon1_BalloonTipClicked;
        }

        private void NotifyIcon1_BalloonTipClicked(object sender, EventArgs e)
        {
            OpenRedRoversLink();
        }

        private void OpenRedRoversLink()
        {
            ProcessStartInfo processStartInfo = new ProcessStartInfo("http://redrovers.com");
            try
            {
                Process.Start(processStartInfo);
            }
            catch (Exception ex)
            {
                Process.Start(@"C:\Program Files\Internet Explorer\iexplore.exe", processStartInfo.FileName);
            }
        }
    }
}
